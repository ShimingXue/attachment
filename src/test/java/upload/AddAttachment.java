package upload;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.junit.Test;

import util.GetFileUtil;
import util.GetPropertiesUtil;

/**
 * 
 * @author Shimix
 *         Mar 9, 2021
 */
public class AddAttachment {
	static Logger log = Logger.getLogger(AddAttachment.class.getName());

	@Test
	public void upload() {
		String url = GetPropertiesUtil.getAttachmentUrl();
		ArrayList<String> projects = GetFileUtil.getDirectorys(url);
		for (String project : projects) {
			ArrayList<String> directoy = GetFileUtil.getDirectorys(project);
			for (String dir : directoy) {
				ArrayList<String> files = GetFileUtil.getFiles(dir);
				for (String file : files) {
					String issueKey = file.split("\\\\")[3];
					execCurl(issueKey, file);
				}
			}
		}

	}

	public static String execCurl(String issueKey, String file) {
		HashMap<String, String> info = GetPropertiesUtil.getInfo();
		String user = info.get("user");
		String password = info.get("password");
		String domain = info.get("domain");

		String[] cmds = { "curl", "-D-", "-u", user + ":" + password, "-X", "POST", "-H", "X-Atlassian-Token: no-check",
				"-F", "file=@" + file, domain + "/rest/api/2/issue/" + issueKey + "/attachments" };
		ProcessBuilder process = new ProcessBuilder(cmds);
		Process p;
		try {
			p = process.start();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			StringBuilder builder = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
				builder.append(System.getProperty("line.separator"));
			}
			String message = builder.toString().split("\\{")[1];
			System.out.println("message: " + message);
			if (message.contains("errors") || "".equals(message)) {
				log.error("upload faild: " + file + "\n" + message);
			}
			log.info("upload success: " + file + "\n" + message);
			return message;
		} catch (Exception e) {
			log.error(e + file);
			e.printStackTrace();
		}
		return null;
	}
}
