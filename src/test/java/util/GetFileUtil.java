package util;

import java.io.File;
import java.util.ArrayList;
/**
 * 
 * @author Shimix
 * Mar 9, 2021
 */
public class GetFileUtil {

	/**
	 * get all directory under path
	 * @param path
	 * @return
	 */
	public static ArrayList<String> getDirectorys(String path) {
		ArrayList<String> directory = new ArrayList<>();
		File file = new File(path);
		File[] tempList = file.listFiles();

		for (int i = 0; i < tempList.length; i++) {
			if (tempList[i].isDirectory()) {
				directory.add(tempList[i].toString());
			}
		}
		return directory;
	}

	/**
	 * get all files under path
	 * @param path
	 * @return
	 */
	public static ArrayList<String> getFiles(String path) {
		ArrayList<String> files = new ArrayList<>();
		File file = new File(path);
		if (file.exists() && file.isDirectory() && file.canRead()) {
			File[] tempList = file.listFiles();
			for (int i = 0; i < tempList.length; i++) {
				if (tempList[i].isFile()) {
					files.add(tempList[i].toString());
				}
			}
			return files;
		}
		return null;
	}
}
