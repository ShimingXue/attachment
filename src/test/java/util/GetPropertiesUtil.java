package util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;
/**
 * 
 * @author Shimix
 * Mar 9, 2021
 */
public class GetPropertiesUtil {

	public static String getAttachmentUrl() {
		Properties props = null;
		InputStream is = null;

		try {
			is = Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties");
			if (is == null) {
				throw new FileNotFoundException("" + "file is not found");
			}
			props = new Properties();
			props.load(is);
			return props.getProperty("attachmentUrl");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return null;
	}
	
	public static String getCsvUrl() {
		Properties props = null;
		InputStream is = null;

		try {
			is = Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties");
			if (is == null) {
				throw new FileNotFoundException("" + "file is not found");
			}
			props = new Properties();
			props.load(is);
			return props.getProperty("csvUrl");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return null;
	}
	
	public static HashMap<String, String> getCsvIndex() {
		HashMap<String, String> map = new HashMap<>();
		Properties props = null;
		InputStream is = null;

		try {
			is = Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties");
			if (is == null) {
				throw new FileNotFoundException("" + "file is not found");
			}
			props = new Properties();
			props.load(is);
			map.put("begin", props.getProperty("startIndex"));
			map.put("end", props.getProperty("endIndex"));
			return map;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return null;
	}
	
	public static HashMap<String, String> getInfo() {
		Properties props = null;
		InputStream is = null;
		HashMap<String, String> info = new HashMap<>();

		try {
			is = Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties");
			if (is == null) {
				throw new FileNotFoundException("" + "file is not found");
			}
			props = new Properties();
			props.load(is);
			info.put("user", props.getProperty("user"));
			info.put("password", props.getProperty("password"));
			info.put("domain", props.getProperty("domain"));
			return info;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return null;
	}
}
