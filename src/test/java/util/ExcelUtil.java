package util;

import java.io.IOException;
import java.util.HashMap;

import com.csvreader.CsvReader;

/**
 * 
 * @author Shimix
 * 		   Mar 12, 2021
 */
public class ExcelUtil {

	/**
	 * translate excel columnIndex to int
	 * @param col
	 * @return
	 */
	public static int getExcelCol(String col) {
		col = col.toUpperCase();
		int count = -1;
		char[] cs = col.toCharArray();
		for (int i = 0; i < cs.length; i++) {
			count += (cs[i] - 64) * Math.pow(26, cs.length - 1 - i);
		}
		return count;
	}
	
	/**
	 * read excel file and return a range about colName which has the same name
	 * @param filePath
	 * @param colName
	 * @return
	 */
	public static HashMap<String, Integer> getColRange(String filePath, String colName) {
		HashMap<String, Integer> map = new HashMap<>();
		int len = 0;
		try {
			CsvReader csvReader = new CsvReader(filePath);
			//read first line
			csvReader.readRecord();
			for(int i = 0;i < csvReader.getColumnCount();i++) {
				if(colName.equals(csvReader.get(i))) {
					map.put("end", i);
					len++;
				}
			}	
		}catch (IOException e) {
			e.printStackTrace();
		}
		map.put("end", map.get("end") + 1);
		map.put("begin", map.get("end") - len);
		return map;
	}
}
