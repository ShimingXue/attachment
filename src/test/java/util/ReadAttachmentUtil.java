package util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.csvreader.CsvReader;
/**
 * 
 * @author Shimix
 *
 * Mar 12, 2021
 */
public class ReadAttachmentUtil {

	/**
	 * read data from csv
	 * @param begin	column start
	 * @param end	column end
	 * @return	result data
	 */
	public static HashMap<String, List<String>> read(String filePath, int begin, int end) {
		HashMap<String, List<String>> map = new HashMap<>();
		try {
			// new CSV read object
			CsvReader csvReader = new CsvReader(filePath);
			// read headline
			csvReader.readHeaders();
			while (csvReader.readRecord()) {
				ArrayList<String> list = new ArrayList<String>();
				//read begin - end column
				for (int i = begin; i < end; i++) {
					String s = csvReader.get(i);
					if (s != null && !"".equals(s)) {
						list.add(s);
					}
				}
				if (!list.isEmpty()) {
					map.put(csvReader.get("Issue key"), list);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return map;
	}
}
