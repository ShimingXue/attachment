package delete;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.junit.Test;

import util.ExcelUtil;
import util.GetFileUtil;
import util.GetPropertiesUtil;
import util.ReadAttachmentUtil;

/**
 * 
 * @author Shimix
 *         Mar 12, 2021
 */
public class DeleteAttachment {
	static Logger log = Logger.getLogger(DeleteAttachment.class.getName());

	@Test
	public void delete() {
		String csvUrl = GetPropertiesUtil.getCsvUrl();
		ArrayList<String> files = GetFileUtil.getFiles(csvUrl);
		for (String file : files) {
			HashMap<String,Integer> colRange = ExcelUtil.getColRange(file, "Attachment");
			HashMap<String, List<String>> attachments = ReadAttachmentUtil.read(file, colRange.get("begin"), colRange.get("end"));
			for (Entry<String, List<String>> entry : attachments.entrySet()) {
				List<String> attachment = entry.getValue();
				for (String str : attachment) {
					String url = str.split(";")[3];
					String attachmentId = url.split("/")[5];
					String info = execCurl(attachmentId);
					log.info(info);
				}
			}
		}

	}

	public static String execCurl(String attachmentId) {
		HashMap<String, String> info = GetPropertiesUtil.getInfo();
		String user = info.get("user");
		String password = info.get("password");
		String domain = info.get("domain");

		String[] cmds = { "curl", "-D-", "-u", user + ":" + password, "-X", "DELETE", "-H",
				"X-Atlassian-Token: no-check", domain + "/rest/api/2/attachment/" + attachmentId };
		ProcessBuilder process = new ProcessBuilder(cmds);
		Process p;
		try {
			p = process.start();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			StringBuilder builder = new StringBuilder();
			builder.append(attachmentId + "\n");
			String line = null;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
				builder.append(System.getProperty("line.separator"));
			}
			String message = builder.toString();
			if (message.contains("errors")) {
				log.error(message);
			}
			log.info(message);

		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
